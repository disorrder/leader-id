// function btoa(str) {
//     // ref https://git.coolaj86.com/coolaj86/btoa.js/src/branch/master/index.js
//     return Buffer.from(str).toString('base64');
// }
// module.exports.btoa = btoa;

function hashCode(str) {
    var char, hash = 0;
	if (str.length == 0) return hash;
	for (let i = 0, len = str.length; i < len; i++) {
		char = str.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
    if (hash < 0) { // to unsigned int32
        let bit32 = 0x80000000;
        hash = bit32 - hash;
    }
	return hash;
}
String.prototype.hashCode = function() {
	return hashCode(this);
};
module.exports.hashCode = hashCode;


// Create an RFC4122 version 4 compliant GUID
function createGuid() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = Math.random() * 16 | 0,
			v = (c == 'x') ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
};
module.exports.createGuid = createGuid;
