// Numbers
const KB = 1000;
const MB = KB * KB;
const GB = MB * KB;
Object.assign(module.exports, {KB, MB, GB});

// Time
const SEC = 1000;
const MIN = 60 * SEC;
const HOUR = 60 * MIN;
const DAY = 24 * HOUR;
const WEEK = 7 * DAY;
const MONTH = 30 * DAY;
const TIMEZONE = new Date().getTimezoneOffset() * MIN;
Object.assign(module.exports, {SEC, MIN, HOUR, DAY, WEEK, MONTH, TIMEZONE});

// Promisify
function sleep(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
};
Object.assign(module.exports, {sleep});
