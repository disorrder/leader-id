// --- Randomize ---
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}
Object.assign(module.exports, {getRandomInt, getRandomFloat});

const availableChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
String.randomize = function(len) {
    var str = "";
    for (let i = 0; i < len; i++) {
        str += availableChars.charAt(Math.floor(Math.random() * availableChars.length));
    }
    return str;
};

Array.prototype.getRandom = function() {
    return this[getRandomInt(0, this.length-1)];
};
