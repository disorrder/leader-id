function assignDeep(target, ...sources) {
    let deepResult = new target.constructor();
    for (let i in target) {
        if (typeof target[i] === "object") {
            let _sources = sources.map((v) => v && v[i]);
            deepResult[i] = assignDeep(target[i], ..._sources);
        }
    }
    return Object.assign(target, ...sources, deepResult);
}

function clearUndefined(target) {
    for (let i in target) {
        if (typeof target[i] === "object") {
            clearUndefined(target[i]);
        }
        if (target[i] === undefined) {
            delete target[i];
        }
    }
}
Object.assignDeep = assignDeep;
module.exports.assignDeep = assignDeep;
module.exports.clearUndefined = clearUndefined;


function traverse(target, cb) {
    if (Array.isArray(target)) {
        target.forEach((v, k) => {
            cb(v, k, target);
            if (typeof v === "object") traverse(v, cb);
        });
        return;
    }
    if (typeof target === "object") {
        Object.keys(target).forEach((k) => {
            let v = target[k];
            cb(v, k, target);
            if (typeof v === "object") traverse(v, cb);
        });
        return;
    }
    cb(target);
}
module.exports.traverse = traverse;
