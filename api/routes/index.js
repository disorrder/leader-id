const Router = require('koa-router');
const router = new Router();
const axios = require("axios");

router.get('/', (ctx, next) => {
    // ctx.router available
    ctx.body = 'Hi there! This is my API. Feel free and good luck! :D';
});

// --- LeaderID ---
let leaderIdConfig = {
    client_id: "378f4e0464325ade400f4a486fc050de",
    client_secret: "a10feee4e6f438ba609bfb2b7333a1e4",
}

router.get("/auth/leaderid/login", async (ctx, next) => {
    let url = new URL("https://leader-id.ru/api/oauth/authorize");
    url.searchParams.set("client_id", leaderIdConfig.client_id);
    url.searchParams.set("redirect_uri", "/api/auth/leaderid/token");
    url.searchParams.set("response_type", "code");
    ctx.redirect(url.href);
});

router.get("/auth/leaderid/token", async (ctx, next) => {
    let {data} = await axios.post("https://leader-id.ru/api/oauth/access_token", {
        params: {
            grant_type: "authorization_code",
            code: ctx.query.code,
            client_id: leaderIdConfig.client_id,
            redirect_uri: "/api/auth/leaderid/callback",
            // state: "123",
        }
    });
    // в data лежат айди юзера и токены.
    // Тут же можно запросить более подробную инфу о юзере, например, уже с токеном.
});

router.get("/auth/leaderid/callback", async (ctx, next) => {
    ctx.redirect("/profile");
});

// --- form ---
router.post("/form", async (ctx) => {
    console.log("post form");
    ctx.body = 123;
});

router.get("/task", async (ctx) => {
    
    let {data} = await axios.get("http://89.208.223.188/task");
    ctx.body = data;
});

module.exports = router;
