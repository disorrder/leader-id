require('./utils');
const cfg = require('../config/api');
{
    let local = {};
    try { local = require("./config"); } catch(e) {}
    let config_ci = process.env.API_CONFIG || {};
    if (typeof config_ci === "string") config_ci = JSON.parse(config_ci);
    global.config = Object.assignDeep(cfg, config_ci, local);
}

// init Koa app
const Koa = require('koa');
const app = new Koa();

const server = require("http").createServer(app.callback());
server.listen(config.port, config.host, function() {
    console.log(`API HTTP server is running on ${config.port} port.`);
});
const httpsServer = require("https").createServer(app.callback());
httpsServer.listen(config.port+1, config.host, function() {
    console.log(`API HTTPS server is running on ${config.port+1} port.`);
});

const localhostIPs = ['::ffff:127.0.0.1', '::1'];
const allowedHosts = ["localhost:8080",];

// Common headers
app.use(async (ctx, next) => {
    if (localhostIPs.includes(ctx.ip) && ctx.get('X-Real-IP')) { // patch for nginx proxy_pass
        ctx.request.ip = ctx.get('X-Real-IP');
        // console.log('IP', ctx.ip, ctx.get("Host"), ctx.get('X-Real-IP'), ctx.get('X-Forwarded-For'));
    }

    if (ctx.host === `localhost:${cfg.port}`) ctx.request.header.host = "localhost:8080";
    if (allowedHosts.includes(ctx.host)) {
        ctx.set('Access-Control-Allow-Origin', ctx.origin);
    }
    ctx.set('Access-Control-Allow-Credentials', 'true');
    ctx.set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'); //?

    try {
        await next();
    } catch(e) {
        e.headers = Object.assign({}, e.headers, ctx.response.headers);
        throw e;
    }
});

const bodyParser = require('koa-body');
app.use(bodyParser({ multipart: true }));

const router = require('./routes');
app
    .use(router.routes())
    .use(router.allowedMethods())
;

module.exports = app;
